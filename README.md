# File Copy Test

로그 유지하고 파일 복사 테스트

[TOC]

## SVN의 파일 복사

`svn copy`로 복사하면 원본 파일과 복사한 파일 모두 로그가 유지됨

## Git의 파일 복사

그런 거 없음. 새 파일 추가

## Git에서 로그 유지하고 파일 복사하는 방법

```bash
git mv README.md README-copy.md
git commit

git checkout HEAD~ README.md
git commit

git checkout -
git merge --no-ff dup
```

[blame](https://gitlab.com/jymaing/file-copy-test/blame/master/README-copy.md)에서 merge commit이 보이지 않는 것을 확인할 수 있음

### 문제점

하나의 파일을 N개 복사하려면 N개 이상의 커밋 필요

커밋 히스토리 더러워짐

rebase하면 어떻게 되는지 확인 필요
