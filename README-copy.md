# File Copy Test

로그 유지하고 복사한 파일

[TOC]

## 복사 방법

[README.md](README.md) 참조

복사한 파일도 [로그가 유지됨](https://gitlab.com/jymaing/file-copy-test/blame/master/README-copy.md)

[원본 파일](https://gitlab.com/jymaing/file-copy-test/commits/master/README.md)과 [복사한 파일](https://gitlab.com/jymaing/file-copy-test/commits/master/README-copy.md) 모두 파일별 커밋 히스토리는 깨끗함
